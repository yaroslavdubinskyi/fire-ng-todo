import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundPageComponent } from '@app/core/containers/not-found-page.component';

import { AuthGuard } from '@app/auth/guards/auth.guard';

const routes: Routes = [
  {
    path: 'todolists',
    loadChildren: './todos/todos.module#TodosModule',
    canActivate: [AuthGuard],
  },
  { path: '404', component: NotFoundPageComponent },
  { path: '', redirectTo: '/todolists', pathMatch: 'full' },
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
