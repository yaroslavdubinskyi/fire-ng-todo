import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { TodolistActions, TodolistActionTypes } from '../actions/todolist';
import { Todolist } from '../models/todolist.interface';

// Entity adapter
export const adaptor = createEntityAdapter<Todolist>();
export interface State extends EntityState<Todolist> {
  error: string;
  creating: boolean;
  isLoading: boolean;
  newTodoList: Todolist;
}

export const initialState: State = adaptor.getInitialState({
  error: '',
  creating: false,
  isLoading: false,
  newTodoList: {
    id: '',
    title: '',
  },
});

export function todolistReducer(state: State = initialState, action: TodolistActions) {
  switch (action.type) {
    case TodolistActionTypes.Query: {
      return {
        ...initialState,
        isLoading: true,
      };
    }

    case TodolistActionTypes.QueryError: {
      return {
        ...state,
        error: action.payload,
        isLoading: false,
      };
    }

    case TodolistActionTypes.Added:
      return adaptor.addOne(action.payload, { ...state, isLoading: false });

    case TodolistActionTypes.Modified:
      return adaptor.updateOne(
        {
          id: action.payload.id,
          changes: action.payload,
        },
        state,
      );

    case TodolistActionTypes.Removed:
      return adaptor.removeOne(action.payload.id, state);

    case TodolistActionTypes.Create:
      return {
        ...state,
        creating: true,
        newTodoList: action.payload,
      };

    case TodolistActionTypes.CreateSuccess:
      return {
        ...state,
        creating: false,
      };

    case TodolistActionTypes.CreateError:
      return {
        ...state,
        creating: false,
        error: action.payload,
      };

    default:
      return state;
  }
}
