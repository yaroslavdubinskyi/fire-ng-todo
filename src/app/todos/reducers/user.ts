import { UserActions, UserActionTypes } from '../actions/user';
import { User } from '@app/auth/models/user.interface';

export interface State {
  users: User[];
  isLoading: boolean;
  error: string;
}

export const initialState: State = {
  isLoading: false,
  error: '',
  users: [],
};

export function usersReducer(state = initialState, action: UserActions): State {
  switch (action.type) {
    case UserActionTypes.GetUsers: {
      return {
        ...state,
        isLoading: true,
        error: '',
      };
    }
    case UserActionTypes.GetUsersSuccess: {
      return {
        ...state,
        isLoading: false,
        users: [...action.payload],
      };
    }

    default: {
      return state;
    }
  }
}
