import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { TodoActions, TodoActionTypes } from '../actions/todo';
import { Todo } from '../models/todo.interface';

// Entity adapter
export const todoAdapter = createEntityAdapter<Todo>();
export interface State extends EntityState<Todo> {
  isLoading: boolean;
}

export const initialState: State = todoAdapter.getInitialState({
  isLoading: false,
});

export function todoReducer(state: State = initialState, action: TodoActions) {
  switch (action.type) {
    case TodoActionTypes.Query: {
      return {
        ...initialState,
        isLoading: true,
      };
    }

    case TodoActionTypes.Added:
      return todoAdapter.addOne(action.payload, { ...state, isLoading: false });

    case TodoActionTypes.Modified:
      return todoAdapter.updateOne(
        {
          id: action.payload.id,
          changes: action.payload,
        },
        state,
      );

    case TodoActionTypes.Removed:
      return todoAdapter.removeOne(action.payload.id, state);

    default:
      return state;
  }
}
