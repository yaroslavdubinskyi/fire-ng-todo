import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromTodo from './todo';
import * as fromTodolist from './todolist';
import * as fromUser from './user';
import * as fromRoot from '@app/reducers';

export interface TodosState {
  todos: fromTodo.State;
  todolists: fromTodolist.State;
  users: fromUser.State;
}

export interface State extends fromRoot.State {
  todos: TodosState;
}

export const reducers: ActionReducerMap<TodosState> = {
  todos: fromTodo.todoReducer,
  todolists: fromTodolist.todolistReducer,
  users: fromUser.usersReducer,
};

export const getTodosState = createFeatureSelector<TodosState>('todos');

export const getTodosEntitiesState = createSelector(getTodosState, state => state.todos);

export const {
  selectIds: getTodosIds,
  selectEntities: getTodosEntities,
  selectAll: getAllTodos,
  selectTotal: getTotalTodos,
} = fromTodo.todoAdapter.getSelectors(getTodosEntitiesState);

export const getTodosLoading = createSelector(getTodosEntitiesState, state => state.isLoading);

export const getTodolistsEntitiesState = createSelector(getTodosState, state => state.todolists);

export const {
  selectIds: getTodolistsIds,
  selectEntities: getTodolistsEntities,
  selectAll: getAllTodolists,
  selectTotal: getTotalTodolists,
} = fromTodolist.adaptor.getSelectors(getTodolistsEntitiesState);

export const getTodolistsLoading = createSelector(
  getTodolistsEntitiesState,
  state => state.isLoading,
);

export const getTodolistsError = createSelector(getTodolistsEntitiesState, state => state.error);

export const getUsersState = createSelector(getTodosState, state => state.users);

export const getUsersList = createSelector(getUsersState, state => state.users);

export const getUsersLoading = createSelector(getUsersState, state => state.isLoading);
