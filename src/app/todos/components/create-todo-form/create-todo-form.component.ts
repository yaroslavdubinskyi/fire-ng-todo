import { Component, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'fire-create-todo-form',
  templateUrl: './create-todo-form.component.html',
  styleUrls: ['./create-todo-form.component.scss'],
})
export class CreateTodoFormComponent implements OnDestroy {
  actionsSubscription: Subscription;
  createTodoForm: FormGroup;

  @Output() create = new EventEmitter<string>();
  constructor(private fb: FormBuilder, route: ActivatedRoute) {
    this.actionsSubscription = route.params
      .pipe(
        map(params => {
          this.createTodoForm = this.fb.group({
            title: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
          });
        }),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }

  createTodo() {
    if (this.createTodoForm.valid) {
      this.create.emit(this.createTodoForm.value.title);
      this.createTodoForm.reset();
    }
  }
}
