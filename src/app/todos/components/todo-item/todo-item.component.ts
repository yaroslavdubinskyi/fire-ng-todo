import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';

import { Todo } from '@app/todos/models/todo.interface';

@Component({
  selector: 'fire-todo-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss'],
})
export class TodoItemComponent implements OnInit {
  @Input() todo: Todo;
  @Output() checkboxClick = new EventEmitter<Todo>();
  @Output() remove = new EventEmitter<Todo>();

  constructor() {}

  get task() {
    return this.todo.task;
  }

  get completed() {
    return this.todo.completed;
  }

  ngOnInit() {}

  toggleCompleted() {
    this.checkboxClick.emit(this.todo);
  }

  removeTodo() {
    this.remove.emit(this.todo);
  }
}
