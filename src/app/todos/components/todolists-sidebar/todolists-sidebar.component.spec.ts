import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialModule } from '@app/material.module';
import { TodolistsSidebarComponent } from './todolists-sidebar.component';

describe('TodolistsSidebarComponent', () => {
  let component: TodolistsSidebarComponent;
  let fixture: ComponentFixture<TodolistsSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule],
      declarations: [TodolistsSidebarComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodolistsSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
