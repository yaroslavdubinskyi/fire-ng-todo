import { Component } from '@angular/core';

@Component({
  selector: 'fire-loading-spinner',
  template: '<mat-spinner diameter="45"></mat-spinner>',
  styles: [
    `
      :host {
        display: flex;
        justify-content: center;
        justify-items: center;
        width: 100%;
        padding: 40px 0;
        backface-visibility: hidden;
      }
    `,
  ],
})
export class LoadingSpinnerComponent {}
