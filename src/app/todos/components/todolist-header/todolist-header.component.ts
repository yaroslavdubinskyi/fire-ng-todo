import {
  Component,
  OnInit,
  OnChanges,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { User } from '@app/auth/models/user.interface';
import { Todolist } from '@app/todos/models/todolist.interface';

@Component({
  selector: 'fire-todolist-header',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './todolist-header.component.html',
  styleUrls: ['./todolist-header.component.scss'],
})
export class TodolistHeaderComponent implements OnInit, OnChanges {
  @Input() todolist: Todolist;
  @Input() users: User[];
  @Output() todolistChange = new EventEmitter<Partial<Todolist>>();
  @Output() remove = new EventEmitter<Todolist>();
  membersSelect = new FormControl('', Validators.required);
  todolistTitle = new FormControl('', Validators.required);

  get title() {
    return this.todolist.title;
  }

  constructor() {}

  ngOnInit() {
    this.membersSelect.setValue(Object.keys(this.todolist.members));
    this.todolistTitle.setValue(this.title);
  }

  ngOnChanges() {
    this.membersSelect.setValue(Object.keys(this.todolist.members));
    this.todolistTitle.setValue(this.title);
  }

  changeMembers() {
    const newTodolist = {
      ...this.todolist,
      members: this.membersSelect.value.reduce((o, val) => {
        o[val] = true;
        return o;
      }, {}),
    };
    this.todolistChange.emit(newTodolist);
  }

  changeTitle() {
    const newTodolist = {
      ...this.todolist,
      title: this.todolistTitle.value,
    };
    this.todolistChange.emit(newTodolist);
  }

  removeTodolist() {
    this.remove.emit(this.todolist);
  }
}
