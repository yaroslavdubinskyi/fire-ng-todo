import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from '@app/material.module';
import { TodolistHeaderComponent } from './todolist-header.component';

describe('TodolistHeaderComponent', () => {
  let component: TodolistHeaderComponent;
  let fixture: ComponentFixture<TodolistHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, ReactiveFormsModule, NoopAnimationsModule],
      declarations: [TodolistHeaderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodolistHeaderComponent);
    component = fixture.componentInstance;
    component.todolist = {
      title: 'Test title',
      members: {
        id1: true,
        id2: true,
      },
    };
    component.users = [
      {
        uid: 'id1',
        email: 'test@mail.com',
        first_name: 'Ivan',
        last_name: 'Ivanov',
      },
      {
        uid: 'id2',
        email: 'test@mail.com',
        first_name: 'Ivan',
        last_name: 'Ivanov',
      },
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
