import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fire-todos-nav-container',
  templateUrl: './todos-nav-container.component.html',
  styleUrls: ['./todos-nav-container.component.scss']
})
export class TodosNavContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
