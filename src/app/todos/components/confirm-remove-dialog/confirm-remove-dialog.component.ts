import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'fire-confirm-remove-dialog',
  templateUrl: './confirm-remove-dialog.component.html',
})
export class ConfirmRemoveDialogComponent {
  constructor(public dialogRef: MatDialogRef<ConfirmRemoveDialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
