import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { switchMap, mergeMap, map, catchError } from 'rxjs/operators';

import { TodosService } from '../services/todos.service';
import { UserActionTypes, GetUsersSuccess, GetUsersError } from '../actions/user';

/**
 * User side effects handler.
 */
@Injectable()
export class UserEffects {
  /**
   * Side effects handler for User GetUsers actions.
   */
  @Effect()
  getUsers$: Observable<Action> = this.actions$.ofType(UserActionTypes.GetUsers).pipe(
    switchMap(action => {
      return this.todosService.getUsersList();
    }),
    map(users => new GetUsersSuccess(users)),
    catchError(err => of(new GetUsersError(err))),
  );

  /**
   * User side effects handler constructor.
   * @constructor
   */
  constructor(private actions$: Actions, private todosService: TodosService) {}
}
