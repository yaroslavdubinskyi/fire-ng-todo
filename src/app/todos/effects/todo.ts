import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { switchMap, mergeMap, map, tap } from 'rxjs/operators';

import { TodosService } from '../services/todos.service';
import { Query, Update, Create, Remove, TodoActionTypes } from '../actions/todo';

/**
 * Todo side effects handler.
 */
@Injectable()
export class TodoEffects {
  /**
   * Side effects handler for Todo Query actions.
   */
  @Effect()
  query$: Observable<Action> = this.actions$.ofType(TodoActionTypes.Query).pipe(
    map((action: Query) => action.payload),
    switchMap(action => {
      return this.todosService.getCurrentUserTodolistTodos(action);
    }),
    mergeMap(actions => actions),
    map((action: any) => {
      return {
        type: `[Todos] ${action.type}`,
        payload: { id: action.payload.doc.id, ...action.payload.doc.data() },
      };
    }),
  );

  /**
   * Side effects handler for Todo Create actions.
   */
  @Effect({ dispatch: false })
  create$: Observable<any> = this.actions$.ofType(TodoActionTypes.Create).pipe(
    map((action: Create) => action.payload),
    switchMap(data => {
      return this.todosService.createTodo(data.listId, data.taskItem);
    }),
  );

  /**
   * Side effects handler for Todo Update actions.
   */
  @Effect({ dispatch: false })
  update$: Observable<any> = this.actions$.ofType(TodoActionTypes.Update).pipe(
    map((action: Update) => action.payload),
    switchMap(data => {
      return this.todosService.updateTodo(data.listId, data.taskId, data.taskChanges);
    }),
  );

  /**
   * Side effects handler for Todo Remove actions.
   */
  @Effect({ dispatch: false })
  remove$: Observable<any> = this.actions$.ofType(TodoActionTypes.Remove).pipe(
    map((action: Remove) => action.payload),
    switchMap(data => {
      return this.todosService.removeTodo(data.listId, data.taskId);
    }),
  );

  /**
   * Todo side effects handler constructor.
   * @constructor
   */
  constructor(private actions$: Actions, private todosService: TodosService) {}
}
