import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { switchMap, mergeMap, map, tap, catchError } from 'rxjs/operators';

import { TodosService } from '../services/todos.service';
import {
  QueryError,
  TodolistActionTypes,
  Create,
  Remove,
  Update,
  CreateSuccess,
} from '../actions/todolist';

/**
 * Todolist side effects handler.
 */
@Injectable()
export class TodolistEffects {
  /**
   * Side effects handler for Todolist Query actions.
   */
  @Effect()
  query$: Observable<Action> = this.actions$.ofType(TodolistActionTypes.Query).pipe(
    switchMap(action => {
      return this.todosService.getCurrentUserTodolists();
    }),
    mergeMap(actions => actions),
    map((action: any) => {
      return {
        type: `[Todolists] ${action.type}`,
        payload: { id: action.payload.doc.id, ...action.payload.doc.data() },
      };
    }),
    catchError(error => of(new QueryError(error.message))),
  );

  /**
   * Side effects handler for Todolist Create actions.
   */
  @Effect()
  create$: Observable<Action> = this.actions$.ofType(TodolistActionTypes.Create).pipe(
    map((action: Create) => action.payload),
    switchMap(todolist => {
      return this.todosService.createTodolist(todolist);
    }),
    map(() => new CreateSuccess()),
  );

  /**
   * Side effects handler for Todolist Update actions.
   */
  @Effect({ dispatch: false })
  update$: Observable<void> = this.actions$.ofType(TodolistActionTypes.Update).pipe(
    map((action: Update) => action.payload),
    switchMap(todolist => {
      return this.todosService.updateTodolist(todolist.id, todolist);
    }),
  );

  /**
   * Side effects handler for Todolist Remove actions.
   */
  @Effect({ dispatch: false })
  remove$: Observable<void> = this.actions$.ofType(TodolistActionTypes.Remove).pipe(
    map((action: Remove) => action.payload),
    tap(() => this.router.navigate(['/'])),
    switchMap(listId => {
      return this.todosService.removeTodolist(listId);
    }),
  );

  /**
   * Todolist side effects handler constructor.
   * @constructor
   */
  constructor(
    private actions$: Actions,
    private todosService: TodosService,
    private router: Router,
  ) {}
}
