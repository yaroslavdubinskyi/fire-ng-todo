import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable, of, from, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import * as fromAuth from '@app/auth/reducers';

import { User } from '@app/auth/models/user.interface';
import { Todolist } from '../models/todolist.interface';
import { Todo } from '../models/todo.interface';
@Injectable()
export class TodosService {
  user$: Observable<User>;
  _user: User;

  /**
   * TodosService constructor.
   * @constructor
   */
  constructor(private afs: AngularFirestore, private store: Store<any>) {
    this.user$ = this.store.pipe(select(fromAuth.getUser));
    this.user$.subscribe((user: User) => {
      this._user = user;
    });
  }

  /**
   * Returns list of all users.
   */
  getUsersList() {
    if (this._user.uid) {
      const usersRef: AngularFirestoreCollection<any> = this.afs.collection<User>('users');
      return usersRef.stateChanges().pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data() as User;
            return data;
          });
        }),
      );
    } else {
      throw new Error('Not Authenticated!');
    }
  }

  /**
   * Returns list of all available for current user todolists.
   */
  getCurrentUserTodolists() {
    if (this._user.uid) {
      const todolistsRef: AngularFirestoreCollection<any> = this.afs.collection<Todolist>(
        'todolists',
        ref => ref.where(`members.${this._user.uid}`, '==', true),
      );
      return todolistsRef.stateChanges();
    } else {
      throw new Error('Not Authenticated!');
    }
  }

  /**
   * Returns list of todos from specified todolist.
   *
   * @param {string} id - The id of the todolist.
   */
  getCurrentUserTodolistTodos(id: string) {
    if (this._user.uid) {
      const todolistsRef: AngularFirestoreCollection<any> = this.afs
        .collection<Todolist>('todolists', ref =>
          ref.where(`members.${this._user.uid}`, '==', true),
        )
        .doc(id)
        .collection<Todo>('todos');

      return todolistsRef.stateChanges();
    } else {
      return throwError('Not Authenticated!');
    }
  }

  /**
   * Creates new todolist.
   *
   * @param {Todolist} todolist - Todolist object without id.
   */
  createTodolist(todolist: Todolist) {
    if (this._user.uid) {
      const todolistId = this.afs.createId();
      const finalTodolist = {
        id: todolistId,
        title: todolist.title,
        members: { ...todolist.members, [this._user.uid]: true },
      };
      const tempTodo = {
        task: 'Write first todo...',
        completed: true,
      } as Todo;

      this.afs
        .collection<Todolist>('todolists')
        .doc(todolistId)
        .set(finalTodolist);
      this.afs.collection<Todo>(`todolists/${todolistId}/todos`).add(tempTodo);

      return of(true);
    } else {
      return throwError('Not Authenticated!');
    }
  }

  /**
   * Updates todolist info.
   *
   * @param {string} listId - Todolist id.
   * @param {Partial<Todolist>} todolist - Partial todolist object with changes.
   */
  updateTodolist(listId: string, listItem: Partial<Todolist>) {
    if (this._user.uid) {
      return from(
        this.afs
          .collection<Todo>(`todolists`)
          .doc(listId)
          .update({
            ...listItem,
            members: {
              ...listItem.members,
              [this._user.uid]: true,
            },
          }),
      );
    } else {
      return throwError('Not Authenticated!');
    }
  }

  /**
   * Removes todolist.
   *
   * @param {string} listId - Todolist id.
   */
  removeTodolist(listId: string) {
    if (this._user.uid) {
      return from(
        this.afs
          .collection<Todolist>(`todolists`)
          .doc(listId)
          .delete(),
      );
    } else {
      return throwError('Not Authenticated!');
    }
  }

  createTodo(listId: string, taskItem: Todo) {
    if (this._user.uid) {
      this.afs.collection<Todo>(`todolists/${listId}/todos`).add(taskItem);
      return of(true);
    } else {
      return throwError('Not Authenticated!');
    }
  }

  /**
   * Updates todo.
   *
   * @param {string} listId - Todolist id.
   * @param {string} taskId - Todo id.
   * @param {Partial<Todo>} taskItem - Partial Todo object with changes.
   */
  updateTodo(listId: string, taskId: string, taskItem: Partial<Todo>) {
    if (this._user.uid) {
      return from(
        this.afs
          .collection<Todo>(`todolists/${listId}/todos`)
          .doc(taskId)
          .update(taskItem),
      );
    } else {
      return throwError('Not Authenticated!');
    }
  }

  /**
   * Removes todo.
   *
   * @param {string} listId - Todolist id.
   * @param {string} taskId - Todo id.
   */
  removeTodo(listId: string, taskId: string) {
    if (this._user.uid) {
      return from(
        this.afs
          .collection<Todo>(`todolists/${listId}/todos`)
          .doc(taskId)
          .delete(),
      );
    } else {
      return throwError('Not Authenticated!');
    }
  }
}
