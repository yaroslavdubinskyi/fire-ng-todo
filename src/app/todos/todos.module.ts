import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { MaterialModule } from '@app/material.module';
import { TodosRoutingModule } from './todos-routing.module';

import { TodolistsPageComponent } from './containers/todolists-page/todolists-page.component';
import { SingleTodolistPageComponent } from './containers/single-todolist-page/single-todolist-page.component';
import { EmptyTodosPageComponent } from './containers/empty-todos-page/empty-todos-page.component';
import { CreateTodolistPageComponent } from './containers/create-todolist-page/create-todolist-page.component';

import { TodolistsSidebarComponent } from './components/todolists-sidebar/todolists-sidebar.component';
import { TodosNavContainerComponent } from './components/todos-nav-container/todos-nav-container.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import { CreateTodoFormComponent } from './components/create-todo-form/create-todo-form.component';
import { ConfirmRemoveDialogComponent } from './components/confirm-remove-dialog/confirm-remove-dialog.component';
import { TodolistHeaderComponent } from './components/todolist-header/todolist-header.component';

import { TodosService } from './services/todos.service';
import { TodoEffects } from './effects/todo';
import { TodolistEffects } from './effects/todolist';
import { UserEffects } from './effects/user';
import { reducers } from './reducers';

export const COMPONENTS = [
  TodolistsPageComponent,
  SingleTodolistPageComponent,
  EmptyTodosPageComponent,
  CreateTodolistPageComponent,
  TodolistsSidebarComponent,
  TodosNavContainerComponent,
  LoadingSpinnerComponent,
  TodoItemComponent,
  CreateTodoFormComponent,
  ConfirmRemoveDialogComponent,
  TodolistHeaderComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TodosRoutingModule,
    StoreModule.forFeature('todos', reducers),
    EffectsModule.forFeature([TodoEffects, TodolistEffects, UserEffects]),
    MaterialModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [TodosService],
  entryComponents: [ConfirmRemoveDialogComponent],
})
export class TodosModule {}
