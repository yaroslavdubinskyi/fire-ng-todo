import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TodolistsPageComponent } from './containers/todolists-page/todolists-page.component';
import { SingleTodolistPageComponent } from './containers/single-todolist-page/single-todolist-page.component';
import { EmptyTodosPageComponent } from './containers/empty-todos-page/empty-todos-page.component';
import { CreateTodolistPageComponent } from './containers/create-todolist-page/create-todolist-page.component';

const routes: Routes = [
  {
    path: '',
    component: TodolistsPageComponent,
    children: [
      {
        path: '',
        component: EmptyTodosPageComponent,
      },
      {
        path: 'create',
        component: CreateTodolistPageComponent,
      },
      {
        path: ':todolistId',
        component: SingleTodolistPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TodosRoutingModule {}
