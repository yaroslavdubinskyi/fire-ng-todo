import { Action } from '@ngrx/store';

import { Todolist } from '../models/todolist.interface';

export enum TodolistActionTypes {
  Query = '[Todolists] Query todolists',
  QueryError = '[Todolists] Query error',
  Added = '[Todolists] added',
  Modified = '[Todolists] modified',
  Removed = '[Todolists] removed',
  Create = '[Todolists] Create todolist',
  CreateSuccess = '[Todolists] Create todolist success',
  CreateError = '[Todolists] Create todolist error',
  Update = '[Todolists] Update todolist',
  Remove = '[Todolists] Remove todolist',
}

export class Query implements Action {
  readonly type = TodolistActionTypes.Query;
}

export class QueryError implements Action {
  readonly type = TodolistActionTypes.QueryError;
  constructor(public payload: string) {}
}

export class Added implements Action {
  readonly type = TodolistActionTypes.Added;
  constructor(public payload: Todolist) {}
}

export class Modified implements Action {
  readonly type = TodolistActionTypes.Modified;
  constructor(public payload: Todolist) {}
}

export class Removed implements Action {
  readonly type = TodolistActionTypes.Removed;
  constructor(public payload: Todolist) {}
}

export class Create implements Action {
  readonly type = TodolistActionTypes.Create;
  constructor(public payload: Todolist) {}
}

export class CreateSuccess implements Action {
  readonly type = TodolistActionTypes.CreateSuccess;
}

export class CreateError implements Action {
  readonly type = TodolistActionTypes.CreateError;
  constructor(public payload: string) {}
}

export class Update implements Action {
  readonly type = TodolistActionTypes.Update;
  constructor(public payload: Partial<Todolist>) {}
}

export class Remove implements Action {
  readonly type = TodolistActionTypes.Remove;
  constructor(public payload: string) {}
}

export type TodolistActions =
  | Query
  | QueryError
  | Added
  | Modified
  | Removed
  | Create
  | CreateSuccess
  | CreateError
  | Update
  | Remove;
