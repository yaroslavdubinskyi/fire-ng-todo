import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Todolist } from '@app/todos/models/todolist.interface';
import * as TodoList from '../../actions/todolist';
import * as User from '../../actions/user';
import * as fromTodolist from '../../reducers';

@Component({
  selector: 'fire-todolists-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './todolists-page.component.html',
  styleUrls: ['./todolists-page.component.scss'],
})
export class TodolistsPageComponent implements OnInit {
  todolists$: Observable<Todolist[]>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<fromTodolist.TodosState>) {}

  ngOnInit() {
    this.todolists$ = this.store.select(fromTodolist.getAllTodolists);
    this.isLoading$ = this.store.select(fromTodolist.getTodolistsLoading);
    this.store.dispatch(new TodoList.Query());
    this.store.dispatch(new User.GetUsers());
  }
}
