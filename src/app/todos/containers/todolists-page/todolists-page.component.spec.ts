import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StoreModule, combineReducers } from '@ngrx/store';

import { MaterialModule } from '@app/material.module';
import { TodolistsPageComponent } from './todolists-page.component';

import { LoadingSpinnerComponent } from '@app/todos/components/loading-spinner/loading-spinner.component';
import { TodosNavContainerComponent } from '@app/todos/components/todos-nav-container/todos-nav-container.component';
import { TodolistsSidebarComponent } from '@app/todos/components/todolists-sidebar/todolists-sidebar.component';

import * as fromRoot from '@app/reducers';
import * as fromTodos from '@app/todos/reducers';

describe('TodolistsPageComponent', () => {
  let component: TodolistsPageComponent;
  let fixture: ComponentFixture<TodolistsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          todos: combineReducers(fromTodos.reducers),
        }),
        RouterModule.forRoot([]),
      ],
      declarations: [
        TodolistsPageComponent,
        TodosNavContainerComponent,
        LoadingSpinnerComponent,
        TodolistsSidebarComponent,
      ],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodolistsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
