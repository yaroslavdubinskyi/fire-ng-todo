import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { User } from '@app/auth/models/user.interface';
import { Todolist } from '@app/todos/models/todolist.interface';
import * as TodolistActions from '@app/todos/actions/todolist';
import * as fromTodos from '../../reducers';

@Component({
  selector: 'fire-create-todolist-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './create-todolist-page.component.html',
  styleUrls: ['./create-todolist-page.component.scss'],
})
export class CreateTodolistPageComponent implements OnInit {
  createTodolistForm: FormGroup;
  users$: Observable<User[]>;

  constructor(private fb: FormBuilder, private store: Store<fromTodos.TodosState>) {}

  ngOnInit() {
    this.users$ = this.store.select(fromTodos.getUsersList);
    this.createTodolistForm = this.fb.group({
      title: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      members: [[], Validators.required],
    });
  }

  createTodolist() {
    if (this.createTodolistForm.valid) {
      const newTodolist = {
        title: this.createTodolistForm.value.title,
        members: this.createTodolistForm.value.members.reduce((o, val) => {
          o[val] = true;
          return o;
        }, {}),
      } as Todolist;
      this.store.dispatch(new TodolistActions.Create(newTodolist));
      this.createTodolistForm.reset();
    }
  }
}
