import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule, combineReducers } from '@ngrx/store';

import { MaterialModule } from '@app/material.module';
import { CreateTodolistPageComponent } from './create-todolist-page.component';

import * as fromRoot from '@app/reducers';
import * as fromTodos from '@app/todos/reducers';

describe('CreateTodolistPageComponent', () => {
  let component: CreateTodolistPageComponent;
  let fixture: ComponentFixture<CreateTodolistPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          todos: combineReducers(fromTodos.reducers),
        }),
        ReactiveFormsModule,
        NoopAnimationsModule,
        RouterModule.forRoot([]),
      ],
      declarations: [CreateTodolistPageComponent],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTodolistPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
