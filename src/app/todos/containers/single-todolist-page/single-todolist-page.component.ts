import { Component, OnDestroy, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { ConfirmRemoveDialogComponent } from '@app/todos/components/confirm-remove-dialog/confirm-remove-dialog.component';

import { User } from '@app/auth/models/user.interface';
import { Todo } from '@app/todos/models/todo.interface';
import { Todolist } from '@app/todos/models/todolist.interface';
import * as TodoActions from '../../actions/todo';
import * as TodolistActions from '../../actions/todolist';
import * as fromTodos from '../../reducers';

@Component({
  selector: 'fire-single-todolist-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './single-todolist-page.component.html',
  styleUrls: ['./single-todolist-page.component.scss'],
})
export class SingleTodolistPageComponent implements OnInit, OnDestroy {
  actionsSubscription: Subscription;
  todolistId: string;
  todolist$: Observable<Todolist>;
  users$: Observable<User[]>;
  todos$: Observable<Todo[]>;
  isLoading$: Observable<boolean>;

  constructor(
    private store: Store<fromTodos.TodosState>,
    route: ActivatedRoute,
    public dialog: MatDialog,
  ) {
    this.actionsSubscription = route.params
      .pipe(
        map(params => {
          this.todolistId = params.todolistId;
          this.store.dispatch(new TodoActions.Query(params.todolistId));
          this.todolist$ = this.store
            .select(fromTodos.getTodolistsEntities)
            .pipe(map(entities => entities[params.todolistId]));
        }),
      )
      .subscribe();
  }
  ngOnInit() {
    this.todos$ = this.store.select(fromTodos.getAllTodos);
    this.users$ = this.store.select(fromTodos.getUsersList);
    this.isLoading$ = this.store.select(fromTodos.getTodosLoading);
  }
  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }

  toggleTodoState(todo: Todo) {
    const taskChanges = {
      completed: !todo.completed,
    };
    this.store.dispatch(
      new TodoActions.Update({
        listId: this.todolistId,
        taskId: todo.id,
        taskChanges,
      }),
    );
  }

  createTodo(task: string) {
    const newTodo = {
      task,
      completed: false,
    } as Todo;
    this.store.dispatch(
      new TodoActions.Create({
        listId: this.todolistId,
        taskItem: newTodo,
      }),
    );
  }

  removeTodo(todo: Todo) {
    const dialogRef = this.dialog.open(ConfirmRemoveDialogComponent, {
      width: '300px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.store.dispatch(
          new TodoActions.Remove({
            listId: this.todolistId,
            taskId: todo.id,
          }),
        );
      }
    });
  }

  updateTodolist(todolist: Partial<Todolist>) {
    this.store.dispatch(new TodolistActions.Update(todolist));
  }

  removeTodolist(todolist: Todolist) {
    const dialogRef = this.dialog.open(ConfirmRemoveDialogComponent, {
      width: '300px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.store.dispatch(new TodolistActions.Remove(todolist.id));
      }
    });
  }
}
