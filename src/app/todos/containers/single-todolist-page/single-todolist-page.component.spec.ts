import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule, combineReducers } from '@ngrx/store';

import { MaterialModule } from '@app/material.module';
import { SingleTodolistPageComponent } from './single-todolist-page.component';

import { LoadingSpinnerComponent } from '@app/todos/components/loading-spinner/loading-spinner.component';
import { CreateTodoFormComponent } from '@app/todos/components/create-todo-form/create-todo-form.component';
import { TodolistHeaderComponent } from '@app/todos/components/todolist-header/todolist-header.component';
import { TodoItemComponent } from '@app/todos/components/todo-item/todo-item.component';

import * as fromRoot from '@app/reducers';
import * as fromTodos from '@app/todos/reducers';

describe('SingleTodolistPageComponent', () => {
  let component: SingleTodolistPageComponent;
  let fixture: ComponentFixture<SingleTodolistPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          todos: combineReducers(fromTodos.reducers),
        }),
        ReactiveFormsModule,
        NoopAnimationsModule,
        RouterModule.forRoot([]),
      ],
      declarations: [
        SingleTodolistPageComponent,
        CreateTodoFormComponent,
        LoadingSpinnerComponent,
        TodolistHeaderComponent,
        TodoItemComponent,
      ],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleTodolistPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
