import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'fire-empty-todos-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './empty-todos-page.component.html',
  styleUrls: ['./empty-todos-page.component.scss'],
})
export class EmptyTodosPageComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
