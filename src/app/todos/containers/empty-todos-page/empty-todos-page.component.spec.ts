import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '@app/material.module';
import { EmptyTodosPageComponent } from './empty-todos-page.component';

describe('EmptyTodosPageComponent', () => {
  let component: EmptyTodosPageComponent;
  let fixture: ComponentFixture<EmptyTodosPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, RouterModule.forRoot([])],
      declarations: [EmptyTodosPageComponent],
      providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyTodosPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
