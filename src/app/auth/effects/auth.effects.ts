import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of, from } from 'rxjs';
import { tap, map, exhaustMap, catchError } from 'rxjs/operators';

import { AuthService } from '@app/auth/services/auth.service';
import {
  Login,
  LoginSuccess,
  LoginFailure,
  Register,
  RegisterSuccess,
  RegisterFailure,
  AuthActionTypes,
} from '../actions/auth';
import { User, EmailAuthData, RegisterData } from '../models/user.interface';

/**
 * Auth side effects handler.
 */
@Injectable()
export class AuthEffects {
  /**
   * Side effects handler for Auth Login actions.
   */
  @Effect()
  login$ = this.actions$.pipe(
    ofType(AuthActionTypes.Login),
    map((action: Login) => action.payload),
    exhaustMap((auth: EmailAuthData) =>
      from(this.authProvider.login(auth)).pipe(
        map((user: User) => new LoginSuccess(user)),
        catchError(error => of(new LoginFailure(error.message))),
      ),
    ),
  );

  /**
   * Side effects handler for Auth LoginSuccess actions.
   */
  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess, AuthActionTypes.RegisterSuccess),
    tap(() => this.router.navigate(['/'])),
  );

  /**
   * Side effects handler for Auth Logout actions.
   */
  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    tap(() => this.authProvider.logout()),
  );

  /**
   * Side effects handler for Auth Register actions.
   */
  @Effect()
  register$ = this.actions$.pipe(
    ofType(AuthActionTypes.Register),
    map((action: Register) => action.payload),
    exhaustMap((auth: RegisterData) =>
      from(this.authProvider.register(auth)).pipe(
        map((user: User) => new RegisterSuccess(user)),
        catchError(error => of(new RegisterFailure(error.message))),
      ),
    ),
  );

  /**
   * Side effects handler for Auth LoginRedirect actions.
   */
  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginRedirect, AuthActionTypes.Logout),
    tap(authed => {
      this.router.navigate(['/login']);
    }),
  );

  /**
   * Auth side effects handler constructor.
   * @constructor
   */
  constructor(
    private actions$: Actions,
    private authProvider: AuthService,
    private router: Router,
  ) {}
}
