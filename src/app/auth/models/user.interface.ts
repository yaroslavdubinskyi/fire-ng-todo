export interface User {
  uid: string;
  email: string;
  display_name?: string;
  first_name?: string;
  last_name?: string;
}

export interface EmailAuthData {
  email: string;
  password: string;
}

export interface RegisterData {
  email: string;
  password: string;
  first_name: string;
  last_name: string;
}
