import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

import { User, EmailAuthData, RegisterData } from '@app/auth/models/user.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(public ofAuth: AngularFireAuth, private afs: AngularFirestore) {}

  /**
   * Returns user authentefication session from Firebase.
   *
   */
  getAuthState() {
    return this.ofAuth.authState;
  }

  /**
   * Signs Up user with email and password
   *
   * @param {RegisterData} user - User information.
   */
  register(user: RegisterData) {
    return this.ofAuth.auth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(credential => {
        const userRef: AngularFirestoreDocument<User> = this.afs.doc(
          `users/${credential.user.uid}`,
        );

        const data: User = {
          uid: credential.user.uid,
          email: user.email,
          first_name: user.first_name,
          last_name: user.last_name,
        };

        userRef.set(data, { merge: true });

        return userRef.ref.get();
      })
      .then(data => data.data());
  }

  /**
   * Signs In user with email and password
   *
   * @param {EmailAuthData} user - User credentials.
   */
  login(user: EmailAuthData) {
    return this.ofAuth.auth
      .signInWithEmailAndPassword(user.email, user.password)
      .then(credential => {
        const userRef: AngularFirestoreDocument<User> = this.afs.doc(
          `users/${credential.user.uid}`,
        );
        return userRef.ref.get();
      })
      .then(ref => {
        return ref.data();
      });
  }

  /**
   * Signs out current user.
   */
  logout() {
    return this.ofAuth.auth.signOut();
  }
}
