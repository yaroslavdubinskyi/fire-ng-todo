import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { EmailAuthData } from '../models/user.interface';
import * as fromAuth from '../reducers';
import * as Auth from '../actions/auth';

@Component({
  selector: 'fire-login-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <fire-login-form
    (submitted)="onSubmit($event)"
    [pending]="pending$ | async"
    [errorMessage]="error$ | async"></fire-login-form>
  `,
  styles: [],
})
export class LoginPageComponent {
  pending$ = this.store.pipe(select(fromAuth.getLoginPagePending));
  error$ = this.store.pipe(select(fromAuth.getLoginPageError));

  constructor(private store: Store<fromAuth.State>) {}

  onSubmit($event: EmailAuthData) {
    this.store.dispatch(new Auth.Login($event));
  }
}
