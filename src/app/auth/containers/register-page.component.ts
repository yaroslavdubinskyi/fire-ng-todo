import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { RegisterData } from '@app/auth/models/user.interface';
import * as fromAuth from '../reducers';
import * as Auth from '../actions/auth';

@Component({
  selector: 'fire-register-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
  <fire-register-form
    (submitted)="onSubmit($event)"
    [pending]="pending$ | async"
    [errorMessage]="error$ | async"></fire-register-form>
  `,
  styles: [],
})
export class RegisterPageComponent {
  pending$ = this.store.pipe(select(fromAuth.getRegisterPagePending));
  error$ = this.store.pipe(select(fromAuth.getRegisterPageError));

  constructor(private store: Store<fromAuth.State>) {}

  onSubmit($event: RegisterData) {
    this.store.dispatch(new Auth.Register($event));
  }
}
