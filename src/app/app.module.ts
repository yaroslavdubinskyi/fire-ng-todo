import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FIREBASE_CONFIG } from '@app/helpers/firebase.config';

import { CoreModule } from '@app/core/core.module';
import { AuthModule } from '@app/auth/auth.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from '@app/core/containers/app.component';
import { environment } from '@env/environment';

import { CustomRouterStateSerializer } from '@app/helpers/router.helpers';
import { reducers, metaReducers } from './reducers';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFirestoreModule,

    StoreModule.forRoot(reducers, { metaReducers }),

    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
    }),

    StoreDevtoolsModule.instrument({
      name: 'Fire-ng-ToDo',
      logOnly: environment.production,
    }),

    EffectsModule.forRoot([]),

    CoreModule.forRoot(),
    AuthModule.forRoot(),

    AppRoutingModule,

    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [{ provide: RouterStateSerializer, useClass: CustomRouterStateSerializer }],
  bootstrap: [AppComponent],
})
export class AppModule {}
