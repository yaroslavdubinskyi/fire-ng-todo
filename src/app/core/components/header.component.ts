import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fire-header',
  template: `
    <mat-toolbar color="primary">
      <ng-content></ng-content>
    </mat-toolbar>
  `,
  styles: []
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
