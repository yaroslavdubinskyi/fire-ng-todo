import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as Auth from '@app/auth/actions/auth';
import * as fromAuth from '@app/auth/reducers';
import { User } from '@app/auth/models/user.interface';

@Component({
  selector: 'fire-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <fire-header>
      <a [routerLink]="['/']" class="fire-logo">Fire-ng-ToDo</a>
      <span class="head-spacer"></span>
      <a *ngIf="!(isLoggedIn$ | async)" [routerLink]="['/login']" routerLinkActive="active" class="menu-item">Login</a>
      <a *ngIf="!(isLoggedIn$ | async)" [routerLink]="['/register']" routerLinkActive="active" class="menu-item">Register</a>
      <a *ngIf="isLoggedIn$ | async">Hi, {{(userInfo$ | async).first_name}}!</a>
      <a *ngIf="isLoggedIn$ | async" (click)="logout()" class="menu-item">Logout</a>
    </fire-header>
    <mat-sidenav-container fullscreen>
      <mat-sidenav mode="side">Sidenav content</mat-sidenav>
      <mat-sidenav-content>
        <router-outlet></router-outlet>
      </mat-sidenav-content>
    </mat-sidenav-container>
  `,
  styles: [
    `
      mat-sidenav-container {
        margin-top: 64px;
      }
      .fire-logo {
        color: #ffffff;
        text-decoration: none;
      }
      .head-spacer {
        flex: 1 1 auto;
      }
      .menu-item {
        font-size: 18px;
        cursor: pointer;
        color: #ffffff;
        text-decoration: none;
        margin-left: 20px;
      }
      .menu-item.active {
        text-decoration: underline;
      }
    `,
  ],
})
export class AppComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;
  userInfo$: Observable<User>;
  constructor(private store: Store<fromAuth.State>) {}

  ngOnInit() {
    this.isLoggedIn$ = this.store.select(fromAuth.getLoggedIn);
    this.userInfo$ = this.store.select(fromAuth.getUser);
  }

  logout() {
    this.store.dispatch(new Auth.Logout());
  }
}
